package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;


public class fastSearchPage {
WebDriver driver;
WebDriverWait wait;
fastSearchLocators fastSearchLocators;
        public fastSearchPage(WebDriver driver){
            this.driver = driver;
            fastSearchLocators = new fastSearchLocators();
            PageFactory.initElements(driver, fastSearchLocators);
            wait = new WebDriverWait(driver, 10);
    }

}
