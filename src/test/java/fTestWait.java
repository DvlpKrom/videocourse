import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Instant;
import java.util.concurrent.TimeUnit;

public class fTestWait {

    private WebDriver driver;


    @Before
    public void setup6() {
        System.setProperty("webdriver.chrome.driver", "C:\\Dell\\Drivers\\MKHCF\\ChromeDriver\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.navigate().to("https://en.wikipedia.org/wiki/Main_Page");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void wikiTest6() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id(""))));
        Assert.assertEquals(driver.getTitle(), "Wikipedia, the free encyclopedia");
        driver.findElement(By.linkText("About Wikipedia")).click();
        verifyElementPresent(driver, By.id(("hasdgahgsda")));
        //Assert.assertEquals(true, verifyElementPresent(driver, By.id("hasdgahgsda")));
        driver.findElement(By.cssSelector("#searchInput")).sendKeys("Warsaw");
        driver.findElement(By.id("searchButton")).click();


    }

    public static boolean verifyElementPresent(WebDriver driver, By by) {

        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Element not found");
            return false;
        }
    }

    @After
    public void tearDown6() {
        driver.quit();
    }

}
