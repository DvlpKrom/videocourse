import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;


public class gPcWrold {
    private WebDriver driver;

    @Before
    public void SetUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\Dell\\Drivers\\MKHCF\\ChromeDriver\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.navigate().to("https://www.currys.co.uk/gbuk/index.html");

    }

    @Test
    public void PCWorldTest() {
        WebDriverWait wait = new WebDriverWait(driver, 10);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("onetrust-group-container")));
        driver.findElement(By.id("onetrust-accept-btn-handler"))
                .click();

        driver.navigate().to("https://www.currys.co.uk/gbuk/tv-and-home-entertainment/televisions-301-c.html?ntid=B%7Ed%7ETelevisions");

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("popular-links")));
        driver.findElement(By.linkText("Smart TVs"))
                .click();

        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("article.product.result-prd")));
        assertEquals(
                29,
                driver.findElements(By.cssSelector("article.product.result-prd"))
                        .size()
        );
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//div[@class='dc-select-input dc-select-valid']")));
        driver.findElement(By.xpath("//div[@class='dc-select-input dc-select-valid']"))
                .click();


        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//li[contains(text(),'Brand - A to Z')]")));
        driver.findElement(By.xpath("//li[contains(text(),'Brand - A to Z')]"))
                .click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='dc-filter-container']/div/nav[2]/ul[1]/li[8]/div[1]")));
        driver.findElement(By.xpath("//div[@class='dc-filter-container']/div/nav[2]/ul[1]/li[8]/div[1]"))
                .click();

        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("col9 .product-list-view")));
        assertEquals(
                14,
                driver.findElements(By.cssSelector("col9 .product-list-view"))
                        .size()
        );
 

    }
//line 57-61 still doesn't work.
    // TODO: 2020-04-17
    @After
    public void TearDown() {

        driver.quit();
    }
}

