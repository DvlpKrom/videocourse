import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class dTestByLinkText {

    private WebDriver driver;

    @Before
    public void setup4() {
        System.setProperty("webdriver.chrome.driver", "C:\\Dell\\Drivers\\MKHCF\\ChromeDriver\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.navigate().to("https://en.wikipedia.org/wiki/Main_Page");
    }

    @Test
    public void wikiTest4() {
        Assert.assertEquals(driver.getTitle(), "Wikipedia, the free encyclopedia");
        driver.findElement(By.linkText("About Wikipedia")).click();
        //driver.findElement(By.linkText("About Wikipedia")).sendKeys("Warsaw");
        driver.findElement(By.id("searchButton")).click();


    }

    @After
    public void tearDown4() {
        driver.quit();
    }


}
