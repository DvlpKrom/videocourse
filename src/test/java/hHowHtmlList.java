import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


public class hHowHtmlList {
    private WebDriver driver;


    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\Dell\\Drivers\\MKHCF\\ChromeDriver\\chromedriver.exe");
        driver = new ChromeDriver();

    }

    @Test
    public void selectMethod() {
        driver.navigate()
                .to("http://how2html.pl/select-html/");
        new Select(driver.findElement(By.id("dessert")))
                .selectByIndex(2);

    }

    @Test
    public void dropDownProduct() {
        driver.navigate().to("https://www.samsung.com/pl/tvs/all-tvs/");
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector(".shop-select__placeholder")));
        driver.findElement(By.cssSelector(".shop-select__placeholder"))
                .click();
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector(".shop-select__options")));
        driver.findElements(By.cssSelector(".shop-select__options li"))
                .get(0)
                .click();
        driver.findElements(By.cssSelector("div.shop-grid .s-txt-title"))
                .get(0)
                .getText()
                .contains("text(), 'Crystal UHD 75 TU7192");


    }

    @After
    public void tearDown() {
        driver.quit();
    }


}
