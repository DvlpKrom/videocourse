import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class bTestByCSS {

    private WebDriver driver;

    @Before
    public void setup2() {
        System.setProperty("webdriver.chrome.driver", "C:\\Dell\\Drivers\\MKHCF\\ChromeDriver\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.navigate().to("https://en.wikipedia.org/wiki/Main_Page");
    }

    @Test
    public void wikiTest2() {
        Assert.assertEquals(driver.getTitle(), "Wikipedia, the free encyclopedia");
        driver.findElement(By.cssSelector(".mw-wiki-logo")).click();
        driver.findElement(By.cssSelector("#searchInput")).sendKeys("Warsaw");
        driver.findElement(By.id("searchButton")).click();


    }

    @After
    public void tearDown2() {
        driver.quit();
    }


}
